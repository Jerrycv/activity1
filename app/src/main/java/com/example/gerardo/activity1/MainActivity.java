package com.example.gerardo.activity1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.Activity1.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }



    public void changeView(View view){
        EditText editText = (EditText) findViewById(R.id.nameText);
        String nameAdventurer = editText.getText().toString();
        Intent intent = new Intent(MainActivity.this,AdventureActivity.class);
        intent.putExtra(EXTRA_MESSAGE, nameAdventurer);
        startActivity(intent);

    }
}
