package com.example.gerardo.activity1;

import android.animation.TimeAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.w3c.dom.Text;
import butterknife.ButterKnife;

public class AdventureActivity extends AppCompatActivity {
    Story story = new Story();
    TextView text;
    Button firstOpt;
    Button secondOpt;
    Page current;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adventure);
        Intent intent = getIntent();
        String nameAdventurer = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        current = story.getPage(0);
        image= (ImageView) findViewById(R.id.imageView2);

        firstOpt = (Button) findViewById(R.id.Opt1);
        secondOpt = (Button) findViewById(R.id.Opt2);
        text = (TextView) findViewById(R.id.storyText);


        firstOpt.setText(story.getPage(0).getChoice1().getText());
        secondOpt.setText(story.getPage(0).getChoice2().getText());

        text.setText(current.getText());



    }


    public void CheckChoice(View view) {

        if (current.isFinal() != true) {
            if (firstOpt.isPressed() == true) {
                current = story.getPage(current.getChoice1().getNextPage());
            }

            if (secondOpt.isPressed() == true) {
                current = story.getPage(current.getChoice2().getNextPage());
            }

            if (current.isFinal() != true) {
                firstOpt.setText(current.getChoice1().getText());
                secondOpt.setText(current.getChoice2().getText());
                text.setText(current.getText());
                image.setImageResource(current.getImageId());

            }
        }
            text.setText(current.getText());

        //I was trying to return to the Main Activity automatically, i couldnt find a method to delay it
            //Intent intent = new Intent(this,MainActivity.class);
            //startActivity(intent);
    }

}
